package pl.akademiakodu.asocjacje.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.asocjacje.dto.TestDto;
import pl.akademiakodu.asocjacje.mapper.TestMapper;
import pl.akademiakodu.asocjacje.repository.TestRepository;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/tests")
public class TestController {
    // https://medium.com/@hantsy/protect-rest-apis-with-spring-security-and-jwt-5fbc90305cc5

    // https://github.com/venustuspl/Quiz2020/tree/master/src/main/java/com/example/Quiz2020

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private TestRepository testRepository;

    @RequestMapping("/{id}")
    public TestDto getTest(@PathVariable Long id){
        return testMapper.mapToTestDto(testRepository.findById(id).get());
    }

}
