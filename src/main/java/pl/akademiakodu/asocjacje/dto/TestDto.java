package pl.akademiakodu.asocjacje.dto;

import lombok.Data;
import pl.akademiakodu.asocjacje.model.quiz.Question;

import java.util.ArrayList;
import java.util.List;

@Data
public class TestDto {

    private String name;

    private List<QuestionDto> questions = new ArrayList<>();
}
