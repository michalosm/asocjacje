package pl.akademiakodu.asocjacje.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionDto {

    public QuestionDto(String content) {
        this.content = content;
    }

    private Long id;

    private String content;

    private List<String> options = new ArrayList<>();

    private String answer;




}
