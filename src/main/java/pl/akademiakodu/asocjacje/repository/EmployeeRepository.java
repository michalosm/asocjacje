package pl.akademiakodu.asocjacje.repository;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.asocjacje.model.many.Employee;

public interface EmployeeRepository extends CrudRepository<Employee,Long> {

  //  Optional<Employee> findByFirstNameAndLastName(String firstName,String lastName);

}
