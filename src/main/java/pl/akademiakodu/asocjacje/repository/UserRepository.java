package pl.akademiakodu.asocjacje.repository;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.asocjacje.model.User;

public interface UserRepository extends CrudRepository<User,Long> {

}
