package pl.akademiakodu.asocjacje.repository;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.asocjacje.model.quiz.Test;

public interface TestRepository extends CrudRepository<Test,Long> {


}
