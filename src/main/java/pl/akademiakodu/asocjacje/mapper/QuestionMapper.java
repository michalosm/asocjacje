package pl.akademiakodu.asocjacje.mapper;


import org.springframework.stereotype.Component;
import pl.akademiakodu.asocjacje.dto.QuestionDto;
import pl.akademiakodu.asocjacje.model.quiz.Answer;
import pl.akademiakodu.asocjacje.model.quiz.Question;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class QuestionMapper {

    public QuestionDto mapToQuestionDto(final Question question) {
        QuestionDto questionDto = new QuestionDto(question.getContent());
        questionDto.setId(question.getId());
        List<Answer> answers = question.getAnswers();
        questionDto.setOptions(answers.stream().
                map(answer -> answer.getName()).collect(Collectors.toList()));
        questionDto.setAnswer(answers.stream().filter(answer -> answer.isCorrect()).findFirst().get().getName());
        return questionDto;
    }

    public List<QuestionDto> mapToListQuestionDto(List<Question> questions){
        return questions.stream().
                map(question -> mapToQuestionDto(question)).
                collect(Collectors.toList());
    }

}
