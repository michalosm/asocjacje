package pl.akademiakodu.asocjacje.model.quiz;


import lombok.Data;
import lombok.NoArgsConstructor;
import pl.akademiakodu.asocjacje.model.EntityBase;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Answer extends EntityBase {


    private String name;

    private boolean correct;

    public Answer(String name, boolean correct) {
        this.name = name;
        this.correct = correct;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    @JoinColumn(name = "question_id")
    @ManyToOne
    private Question question;




}
