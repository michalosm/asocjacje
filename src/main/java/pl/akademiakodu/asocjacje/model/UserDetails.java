package pl.akademiakodu.asocjacje.model;

import lombok.Data;

import javax.persistence.*;


@Table(name = "details")
@Entity
@Data
public class UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String lastName;

    private String address;

    @OneToOne(mappedBy = "details")
    private User user;

}
