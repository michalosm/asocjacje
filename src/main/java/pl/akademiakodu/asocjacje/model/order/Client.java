package pl.akademiakodu.asocjacje.model.order;

import lombok.Data;
import pl.akademiakodu.asocjacje.model.EntityBase;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Client extends EntityBase {

    private String name;

    private String surname;

    @OneToMany(mappedBy = "client",cascade = CascadeType.ALL)
    private List<Order> orders = new ArrayList<>();


    public void addOrder(Order order){
        orders.add(order);
        order.setClient(this);
    }

}
