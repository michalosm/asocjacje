package pl.akademiakodu.asocjacje.model.order;

import lombok.Data;
import pl.akademiakodu.asocjacje.model.EntityBase;

import javax.persistence.*;


@Data
@Entity
@Table(name = "client_order")
public class Order extends EntityBase {


    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @Column(name = "product_name")
    private String productName;


    private Double price;


}
