package pl.akademiakodu.asocjacje.model.many;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.akademiakodu.asocjacje.model.EntityBase;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;




@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Employee extends EntityBase {


    private String firstName;

    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Employee getMenager() {
        return menager;
    }

    public void setMenager(Employee menager) {
        this.menager = menager;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    @ManyToOne
    private Employee menager;


    @OneToMany(mappedBy = "menager",fetch = FetchType.EAGER)
    private Set<Employee> employees = new HashSet<>();




}
