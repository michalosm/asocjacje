package pl.akademiakodu.asocjacje.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 2 podstawowe rzeczy czy unikalne i czy nullem
    // resztę pokażemy później
    @Column(unique = true,nullable = false)
    private String username;
    // https://www.baeldung.com/javax-validation
    @Column(unique = true,nullable = false)
    private String email;

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "user_details")
    private UserDetails details;




}
