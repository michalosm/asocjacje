package pl.akademiakodu.asocjacje.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.akademiakodu.asocjacje.model.many.Employee;
import pl.akademiakodu.asocjacje.repository.EmployeeRepository;

@Component
public class EmployeeData implements CommandLineRunner {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void run(String... args) throws Exception {
       Employee employee =  Employee.builder().
                firstName("Adam").lastName("Kowalski").build();
        Employee employee2 =  Employee.builder().
                firstName("Piotr").lastName("Nowak").build();

       Employee secondEmployee = Employee.builder().
               firstName("Steve").lastName("Jobs").build();
       employeeRepository.save(secondEmployee);
       employee2.setMenager(secondEmployee);
       employee.setMenager(secondEmployee);
       employeeRepository.save(employee);
       employeeRepository.save(employee2);

       Employee employeeFind = employeeRepository.findById(1L).get();
       System.out.println("Liczba pracownikow to "+employeeFind.getEmployees().size());




    }
}
