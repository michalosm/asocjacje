package pl.akademiakodu.asocjacje.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.akademiakodu.asocjacje.model.order.Client;
import pl.akademiakodu.asocjacje.model.order.Order;
import pl.akademiakodu.asocjacje.repository.ClientRepository;

@Component
public class OrderData implements CommandLineRunner {

    @Autowired
    private ClientRepository clientRepository;


//    private OrderRepository orderRepository;

    @Override
    public void run(String... args) throws Exception {
        Client client = new Client();

        client.setName("Piotr");
        client.setSurname("Kowalski");

        Order order1 = new Order();
        order1.setProductName("Pralka");

        Order order2 = new Order();
        order2.setProductName("Laptop");

        client.addOrder(order1);
        client.addOrder(order2);

        clientRepository.save(client);

    }
}
