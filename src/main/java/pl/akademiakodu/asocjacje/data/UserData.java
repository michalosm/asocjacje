package pl.akademiakodu.asocjacje.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.akademiakodu.asocjacje.model.User;
import pl.akademiakodu.asocjacje.model.UserDetails;
import pl.akademiakodu.asocjacje.repository.UserRepository;

@Component
public class UserData implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setEmail("michalos@wp.pl");
        user.setUsername("adam");
        UserDetails userDetails = new UserDetails();
        userDetails.setAddress("Warszawa Poprawna 14");
        user.setDetails(userDetails);
        userRepository.save(user);
//        userRepository.deleteById(1L);

//        User user1 = new User();
//        user.setUsername("aa");
//        userRepository.save(user);



    }
}
